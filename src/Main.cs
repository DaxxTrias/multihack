﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Forms;
using System.Threading;

namespace Multihack
{
    public partial class Main : Form
    {
        #region --- Global Declares ---

        private readonly Timer _timer = new Timer();
        private readonly MemoryReader _mem = new MemoryReader();
        private readonly Stopwatch _stopwatch = new Stopwatch();
        private Process[] _myProcess = null;
        private ProcessModule MainModule = null;
        public bool Steam = false;
        public bool _gameFound = false;
        public bool lowlifestatus = false;
        public bool brightstatus = false;

        #endregion ---Declares ---

        #region --- Non Steam Related Declares ---

        private const int _ns_xpBasePtr = (0x7D23FC);
        private const int _ns_logoffBasePtr = (0x7D23FC);
        private const int _ns_zoomstepBasePtr = (0x7FA5D8);
        private IntPtr _ns_lowlifeBase = (IntPtr) (0x004C232F);

        private readonly PlayerData _ns_logoffBaseOffsets = new PlayerData(_ns_logoffBasePtr,
                                                                           new[] {0x50, 0x88, 0x17C, 0x788, 0x26F8});
        private readonly PlayerData _ns_zoomstepBaseOffsets = new PlayerData(_ns_zoomstepBasePtr, new[] {0x1698});
        private readonly PlayerData _ns_xpBasePtrOffsets = new PlayerData(_ns_xpBasePtr,
                                                                          new[] {0x50, 0x88, 0x7C, 0x7C, 0x7F4});

        #endregion

        #region --- Steam Related Declares---

        private const int _s_xpBasePtr = (0x7D53FC);
        private const int _s_logoffBasePtr = (0x7D53FC);
        private const int _s_zoomstepBasePtr = (0xFD5F8);

        private readonly PlayerData _s_logoffBaseOffsets = new PlayerData(_s_logoffBasePtr,
                                                                          new[] {0x50, 0x88, 0x17C, 0x788, 0x26F8});

        private readonly PlayerData _s_zoomstepBaseOffsets = new PlayerData(_s_zoomstepBasePtr, new[] {0x16B0});

        private readonly PlayerData _s_xpBasePtrOffsets = new PlayerData(_s_xpBasePtr,
                                                                         new[] {0x50, 0x88, 0x7c, 0x7c, 0x7f4});

        #endregion

        #region --Arrays / Structs--

        private uint[] _playerlowlifeAOB = new uint[] {0xF3, 0x0F, 0x59, 0x44, 0x24, 0x18, 0xF3, 0x59, 0x25};
        private byte[] _playerlowlifeORG = new byte[] {0xF3, 0x0F, 0x59, 0x44, 0x24, 0x18};
        private byte[] _playerlowlifeNOP = new byte[] {0x90, 0x90, 0x90, 0x90, 0x90, 0x90};

    #endregion --structs--

        #region ----Activation events----
        private void comboBoxGameChoice_KeyDown(object sender, KeyEventArgs e)
        {
            GameChoiceUpdate();
            //Thread.Sleep(10);
        }
        private void comboBoxGameChoice_KeyUp(object sender, KeyEventArgs e)
        {
            GameChoiceUpdate();
            Thread.Sleep(100);
        }
        private void comboBoxGameChoice_Click(object sender, EventArgs e)
        {
            GameChoiceUpdate();
        }
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            SteamChoiceUpdate();
        }
        private void comboBoxGameChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            // at some point, try to migrate this over to the closed func() section, to make this just an activator
            try
            {
                for (int i = 0; i < _myProcess.Length; i++)
                {
                    //Debug.Print(comboBoxGameChoice.Text.Replace(myProcess[i].ProcessName + "-", ""));
                    if (comboBoxGameChoice.SelectedItem.ToString().Contains(_myProcess[i].ProcessName))
                    {
                        _myProcess[0] = Process.GetProcessById(int.Parse(
                            comboBoxGameChoice.Text.Replace(_myProcess[i].
                            ProcessName + "-", "")));

                        MainModule = _myProcess[0].MainModule;
                        _mem.ReadProcess = _myProcess[0];
                        _mem.OpenProcess();
                        _gameFound = true;
                        labelGameStatus.Text = @"Ready";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not connect to the process. \n\n Error Report: \n " + ex.Message);
                labelGameStatus.Text = @"Not Ready";
                comboBoxGameChoice.Items.Clear();
                _myProcess = null;
                // throw;
            }
        }
        private void checkBoxPlayerGameState_CheckedChanged(object sender, EventArgs e)
        {
            PlayerLogOff();
        }
        private void button_FullBright_Click(object sender, EventArgs e)
        {
            PlayerBrightness();
        }
        #endregion

        #region ----Mid Functions----
        private void ResetChoices(int options)
        {
            int buffer = options;

            if (buffer == 0) // Reset minimum to avoid crashes (such as process changing)
            {
                comboBoxGameChoice.Items.Clear();
                _myProcess = null;
            }
            if (buffer == 1) // Reset process / hack related vars
            {
                comboBoxGameChoice.Items.Clear();
                _myProcess = null;
                _gameFound = false;
            }
            if (buffer == 2) // Reset strings and labels
            {
                labelPlayerXP.Text = @"?????";
                labelGameStatus.Text = @"Not Ready!";
                labelPlayerZoomStep.Text = "?????";
                
            }
            if (buffer == 3) //Reset everything
            {
                comboBoxGameChoice.Text = "";
                comboBoxGameChoice.Items.Clear();
                _myProcess = null;
                _gameFound = false;
                labelPlayerXP.Text = @"?????";
                labelPlayerZoomStep.Text = "?????";
                labelFullBrightStatus.Text = "Off";
                labelGameStatus.Text = @"Not Ready!";
            }
        }
        private void SteamChoiceUpdate()
        {
            ResetChoices(3);

            if (checkBoxSteamChoice.Checked)
            {
                Steam = true;
            }
            else
            {
                Steam = false;
            }
        }
        private void GameChoiceUpdate()
        {
            ResetChoices(0);

            if (Steam)
            {
                _myProcess = Process.GetProcessesByName("PathOfExileSteam");
                int proLen = _myProcess.Length;
                if (proLen == 0)
                {
                    _myProcess = null;
                    _gameFound = false;
                }
            }
            else if (Steam == false)
            {
                _myProcess = Process.GetProcessesByName("PathOfExile");
                int proLen = _myProcess.Length;
                if (proLen == 0)
                {
                    _myProcess = null;
                    _gameFound = false;
                }
            }

            if (_myProcess != null)
            {
                foreach (Process t in _myProcess)
                {
                    comboBoxGameChoice.Items.Add(t.ProcessName + "-" + t.Id);
                }
            }
            else
            {
                comboBoxGameChoice.Items.Clear();
            }
        }
        private void PlayerLogOff()
        {
            if (_gameFound == true && _myProcess != null)
            {
                int logout = 1; // variable to commence the logout
                if (Steam)
                {
                    if (checkBoxPlayerGameState.Checked)
                    {
                        SetGameState(_s_zoomstepBaseOffsets, logout);
                        checkBoxPlayerGameState.Checked = false;
                    }
                }
                else if (!Steam)
                {
                    if (checkBoxPlayerGameState.Checked)
                    {
                        SetGameState(_ns_zoomstepBaseOffsets, logout);
                        checkBoxPlayerGameState.Checked = false;
                       
                    }
                }
            }
        }
        private void PlayerBrightness()
        {
            if (_gameFound == true && _myProcess != null)
            {
                if (Steam)
                {
                    //SetFullBright(_s_lowlifeBase, _playerlowlifeNOP);
                }
                else if (!Steam)
                {
                    /*##################
                        //RESET
                        //################
                    if (lowlifestatus && checkBoxlowlifebright.Checked == false)
                    {
                        PokeArray(_ns_lowlifeBase, _playerlowlifeORG);
                        labelFullBrightStatus.Text = "Off";
                        lowlifestatus = false;
                    }
                    /*##################
                    //POKE
                    //################
                    if (checkBoxlowlifebright.Checked && lowlifestatus == false)
                    {
                        PokeArray(_ns_lowlifeBase, _playerlowlifeNOP);
                        labelFullBrightStatus.Text = "On";
                        lowlifestatus = true;
                    }
                    */
                    //if (checkBoxnomist.Chcked = true)
                }
            }
        }
        #endregion

        #region ----Hack Functions----
        private void SetGameState(PlayerData player, int var)
        {
            if (Steam)
            {
                int gamestate = _mem.ReadMultiLevelPointer(
                    _myProcess[0].MainModule.BaseAddress.ToInt32() + _s_logoffBaseOffsets.BaseAddress, 4,
                    _s_logoffBaseOffsets.MultiLevel);
                _mem.WriteInt(gamestate, var);
            }
            else if (!Steam)
            {
                int gamestate = _mem.ReadMultiLevelPointer(
                    _myProcess[0].MainModule.BaseAddress.ToInt32() + _ns_logoffBaseOffsets.BaseAddress, 4,
                    _ns_logoffBaseOffsets.MultiLevel);
                _mem.WriteInt(gamestate, var);
            }
        }
        private void PokeArray(IntPtr address, byte[] var)
        {
            IntPtr bufferaddress = address;
            byte[] buffervar = var;
            int written = buffervar.Length;
            
            _mem.WriteMemArray(bufferaddress, buffervar, out written);
        }
        #endregion

        #region -- Core Functions ---
        public Main()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _timer.Mode = TimerMode.Periodic;
            _timer.Period = 150;
            _timer.Resolution = 1;
            _timer.SynchronizingObject = this;

            _timer.Tick += timer1_Tick;

            _timer.Start();
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            labelTimerSpeed.Text = _stopwatch.ElapsedMilliseconds.ToString(CultureInfo.InvariantCulture);
            _stopwatch.Restart();

            try
            {
                if (_gameFound)
                {
                    if (Steam)
                    {
                        UpdatePlayerInfo();
                        labelPlayerXP.Text = _s_xpBasePtrOffsets._xp.ToString("#,##0");
                        labelPlayerZoomStep.Text = _s_zoomstepBaseOffsets._zoomstep.ToString();
                    }
                    else if (Steam == false)
                    {
                        UpdatePlayerInfo();
                        labelPlayerXP.Text = _ns_xpBasePtrOffsets._xp.ToString("#,##0");
                        labelPlayerZoomStep.Text = _ns_zoomstepBaseOffsets._zoomstep.ToString("#.#0");
                    }

                    // Maybe insert a keystate checker for hotkeys?
                    // CheckInput()
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error in the timer function (1): \n" + ex.Message);
            }

            try
            {
                if (_myProcess != null)
                {
                    if (_myProcess[0].HasExited)
                        _gameFound = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error in the timer function (2): \n" + ex.Message);
                throw;
            }
        }
        private void UpdatePlayerInfo()
        {
            if (Steam)
            {
                int xpBase = _mem.ReadMultiLevelPointer(
                    _myProcess[0].MainModule.BaseAddress.ToInt32() + _s_xpBasePtrOffsets.BaseAddress, 4,
                    _s_xpBasePtrOffsets.MultiLevel);
                int zoomBase =
                    _mem.ReadMultiLevelPointer(
                        _myProcess[0].MainModule.BaseAddress.ToInt32() + _s_zoomstepBaseOffsets.BaseAddress, 1,
                        _s_zoomstepBaseOffsets.MultiLevel);


                _s_zoomstepBaseOffsets.UpdateZoom(_mem.ReadFloat(zoomBase));
                _s_xpBasePtrOffsets.UpdateXP(_mem.ReadInt(xpBase));
            }
            else if (Steam == false)
            {
                int xpBase = _mem.ReadMultiLevelPointer(
                    _myProcess[0].MainModule.BaseAddress.ToInt32() + _ns_xpBasePtrOffsets.BaseAddress, 4,
                    _ns_xpBasePtrOffsets.MultiLevel);
                int zoomBase =
                    _mem.ReadMultiLevelPointer(
                    _myProcess[0].MainModule.BaseAddress.ToInt32() + _ns_zoomstepBaseOffsets.BaseAddress, 4,
                    _ns_zoomstepBaseOffsets.MultiLevel);

                _ns_zoomstepBaseOffsets.UpdateZoom(_mem.ReadFloat(zoomBase));
                _ns_xpBasePtrOffsets.UpdateXP(_mem.ReadInt(xpBase));
            }
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            ResetChoices(3);
            _timer.Stop();
        }
        #endregion
        
        
    }
}