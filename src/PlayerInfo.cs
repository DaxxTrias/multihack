﻿namespace Multihack
{
    // Store the pointers
    public class PlayerData
    {
        public PlayerData(int baseAddress, int[] multiLevel)
        {
            BaseAddress = baseAddress;
            MultiLevel = multiLevel;
        }

        public int BaseAddress;
        public int[] MultiLevel;

        public int _xp = 0;
        public float _zoomstep = 0;
        public int _gamestate = 0;

        public void UpdateXP(int xp)
        {
            _xp = xp;
        }
        public void UpdateZoom(float zoomstep)
        {
            _zoomstep = zoomstep;
        }
        public void UpdateGameState(int gamestate)
        {
            _gamestate = gamestate;
        }
    }
}