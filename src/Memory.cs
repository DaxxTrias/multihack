﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Text;

namespace Multihack
{
    internal class MemoryApi
    {
        #region ----Defines----
        public const uint ProcessVmRead = (0x0010);
        public const uint ProcessVmWrite = (0x0020);
        public const uint ProcessVmOperation = (0x0008);
        public const uint PageReadWrite = (0x0004);
        #endregion

        #region ----Win32 API Import Wrappers----
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern Int32 ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, [In, Out] byte[] buffer,
                                                        UInt32 size, out IntPtr lpNumberOfBytesRead);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern Int32 WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, [In, Out] byte[] buffer,
                                                        UInt32 size, out IntPtr lpNumberOfBytesWritten);

        [DllImport("kernel32", SetLastError = true)]
        public static extern IntPtr VirtualAllocEx(IntPtr hProcess, IntPtr lpAddress, UInt32 dwSize, uint flAllocationType, uint flProtect);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool VirtualProtectEx(IntPtr hProcess, IntPtr lpAddress, UInt32 dwSize, uint flNewProtect, out uint lpflOldProtect);


        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr OpenProcess(UInt32 dwDesiredAccess, Int32 bInheritHandle, UInt32 dwProcessId);

        [DllImport("kernel32.dll")]
        public static extern Int32 CloseHandle(IntPtr hObject);
    }
        #endregion

    public class MemoryReader
    {
        #region ---DECLARES----
        private Process _mReadProcess = null;
        private IntPtr _mHProcess = IntPtr.Zero;
        #endregion

        #region ----PROCESS HANDLERS----
        public Process ReadProcess
        {
            get
            {
                return _mReadProcess;
            }
            set
            {
                _mReadProcess = value;
            }
        }

        public void OpenProcess()
        {
            _mHProcess = MemoryApi.OpenProcess(
                MemoryApi.ProcessVmRead | MemoryApi.ProcessVmWrite | MemoryApi.ProcessVmOperation, 1,
                (uint)_mReadProcess.Id);
        }

        public void CloseHandle()
        {
            int iRetValue = MemoryApi.CloseHandle(_mHProcess);
            if (iRetValue == 0)
                throw new Exception("CloseHandle failed");
        }
        #endregion

        #region ----MISC Functions----
        public static string ToHex(int Decimal)
        {
            return Decimal.ToString("X"); //Convert Decimal to Hexadecimal
        }

        public static int ToDec(string Hex)
        {
            return int.Parse(Hex, NumberStyles.HexNumber); //Convert Hexadecimal to Decimal
        }
        #endregion

        #region ----MEMORY READERS----
        public int ReadMem(int memoryAddress, uint bytesToRead, out byte[] buffer)
        {
            IntPtr procHandle = MemoryApi.OpenProcess(
                MemoryApi.ProcessVmRead | MemoryApi.ProcessVmWrite | MemoryApi.ProcessVmOperation, 1,
                (uint)_mReadProcess.Id);

            if (procHandle == IntPtr.Zero)
            {
                buffer = new byte[0];
                return 0;
            }

            buffer = new byte[bytesToRead];
            IntPtr ptrBytesAlreadyRead;
            MemoryApi.ReadProcessMemory(procHandle, (IntPtr)memoryAddress, buffer, bytesToRead, out ptrBytesAlreadyRead);
            MemoryApi.CloseHandle(procHandle);
            return ptrBytesAlreadyRead.ToInt32();
        }
        public int ReadMultiLevelPointer(int memoryAddress, uint bytesToRead, Int32[] offsetList)
        {
            IntPtr procHandle = MemoryApi.OpenProcess(
                MemoryApi.ProcessVmRead | MemoryApi.ProcessVmWrite | MemoryApi.ProcessVmOperation, 1,
                (uint)_mReadProcess.Id);

            IntPtr pointer = (IntPtr)0x0;

            if (procHandle == IntPtr.Zero)
            {
                return 0;
            }

            byte[] btBuffer = new byte[bytesToRead];
            IntPtr lpOutStorage = IntPtr.Zero;

            int pointerAddress = memoryAddress;
            for (int i = 0; i < (offsetList.Length); i++)
            {
                if (i == 0)
                {
                    MemoryApi.ReadProcessMemory(
                        procHandle, (IntPtr)(pointerAddress),
                        btBuffer, (uint)btBuffer.Length,
                        out lpOutStorage);
                }
                pointerAddress = (BitConverter.ToInt32(btBuffer, 0) + offsetList[i]);

                MemoryApi.ReadProcessMemory(
                    procHandle, (IntPtr)(pointerAddress),
                    btBuffer, (uint)btBuffer.Length,
                    out lpOutStorage);
            }
            return pointerAddress;
        }
        public int ReadInt(int memoryAddress)
        {
            byte[] buffer;
            int read = ReadMem(memoryAddress, 4, out buffer);
            if (read == 0)
                return 0;
            else
                return BitConverter.ToInt32(buffer, 0);
        }
        public uint ReadUInt(int memoryAddress)
        {
            byte[] buffer;
            int read = ReadMem(memoryAddress, 4, out buffer);
            if (read == 0)
                return 0;
            else
                return BitConverter.ToUInt32(buffer, 0);
        }
        public byte ReadByte(int memoryAddress)
        {
            byte[] buffer;
            int read = ReadMem(memoryAddress, 1, out buffer);
            if (read == 0)
                return new byte();
            else
                return buffer[0];
        }
        public float ReadFloat(int memoryAddress)
        {
            byte[] buffer;
            int read = ReadMem(memoryAddress, 4, out buffer);
            if (read == 0)
                return 0;
            else
                return BitConverter.ToSingle(buffer, 0);
        }
        public string ReadString(int memoryAddress, uint bytesToRead)
        {
            byte[] buffer;
            int read = ReadMem(memoryAddress, bytesToRead, out buffer);
            if (read == 0)
                return 0.ToString();
            else
                return Encoding.ASCII.GetString(buffer);
        }
        public byte[] ReadMemArray(IntPtr memoryAddress, uint bytesToRead, out int bytesAlreadyRead)
        {
            byte[] buffer = new byte[bytesToRead];

            IntPtr ptrbytesAlreadyRead;
            MemoryApi.ReadProcessMemory(_mHProcess, memoryAddress, buffer, bytesToRead, out ptrbytesAlreadyRead);
            bytesAlreadyRead = ptrbytesAlreadyRead.ToInt32();
            return buffer;
        }

        #endregion

        #region ----MEMORY WRITERS----
        public int WriteMem(int memoryAddress, byte[] buffer)
        {
            IntPtr procHandle =
                MemoryApi.OpenProcess(
                    MemoryApi.ProcessVmRead | MemoryApi.ProcessVmWrite | MemoryApi.ProcessVmOperation, 1,
                    (uint)_mReadProcess.Id);
            
            if (procHandle == IntPtr.Zero)
                return 0;

            uint oldProtect;
            MemoryApi.VirtualProtectEx(procHandle, (IntPtr)memoryAddress, (uint)buffer.Length, MemoryApi.PageReadWrite,
                                       out oldProtect);
            IntPtr ptrBytesWritten;
            MemoryApi.WriteProcessMemory(procHandle, (IntPtr)memoryAddress, buffer, (uint)buffer.Length,
                                         out ptrBytesWritten);
            MemoryApi.CloseHandle(procHandle);
            return ptrBytesWritten.ToInt32();
        }
        public void WriteByte(int memoryAddress, byte b)
        {
            WriteMem(memoryAddress, new[] { b });
        }
        public void WriteInt(int memoryAddress, int w)
        {
            byte[] buffer = BitConverter.GetBytes(w);
            WriteMem(memoryAddress, buffer);
        }
        public void WriteUInt(int memoryAddress, uint u)
        {
            byte[] buffer = BitConverter.GetBytes(u);
            WriteMem(memoryAddress, buffer);
        }
        public void WriteFloat(int memoryAddress, float f)
        {
            byte[] buffer = BitConverter.GetBytes(f);
            WriteMem(memoryAddress, buffer);
        }
        public void WriteMemArray(IntPtr memoryAddress, byte[] bytesToWrite, out int bytesWritten)
        {
            IntPtr ptrBytesWritten;
            MemoryApi.WriteProcessMemory(_mHProcess, memoryAddress, bytesToWrite, (uint)bytesToWrite.Length,
                                         out ptrBytesWritten);
            bytesWritten = ptrBytesWritten.ToInt32();
        }
        #endregion
    }
}